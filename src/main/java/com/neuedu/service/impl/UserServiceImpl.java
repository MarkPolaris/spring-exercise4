package com.neuedu.service.impl;/**
 * @Author: MARK
 * @Date: 2019/7/27 14:10
 * @version: 1.0.0
 * @Description:
 */

import com.neuedu.service.UserService;
import org.springframework.stereotype.Service;

/**
 * @Author: MARK
 * @Date: 2019/7/26 19:46
 * @version: 1.0.0
 * @Description:
 */
@Service
public class UserServiceImpl implements UserService {
    @Override
    public Integer add(Integer a, Integer b) {
        return a + b;
    }
}
