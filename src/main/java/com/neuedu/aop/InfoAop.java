package com.neuedu.aop;/**
 * @Author: MARK
 * @Date: 2019/7/27 14:27
 * @version: 1.0.0
 * @Description:
 */

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.aopalliance.intercept.Joinpoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

/**
 * @Author: MARK
 * @Date: 2019/7/26 19:46
 * @version: 1.0.0
 * @Description:
 */
//将切面类加载到容器中
@Component
//标注该类时切面类
@Aspect
@Slf4j
public class InfoAop {
    @Pointcut("execution(* com.neuedu.*..*(..))")
    private void anyMethod(){}

    @Before(value = "anyMethod()")
    public void before(){
        log.info("前置通知");
    }

    @AfterReturning(value = "anyMethod()", returning = "result")
    public void afterReturning(Integer result){
        log.info("返回值通知，返回值为"+result);
    }

    @After(value = "anyMethod()")
    public void after(){
        log.info("最终通知");
    }

    @Around(value = "anyMethod()")
    public Object around(ProceedingJoinPoint proceedingJoinPoint) throws Throwable{
        String clazz = proceedingJoinPoint.getTarget().getClass().getName();
        String method = proceedingJoinPoint.getSignature().getName();
        String args = JSON.toJSONString(proceedingJoinPoint.getArgs());
        long start = System.currentTimeMillis();
        Object object = proceedingJoinPoint.proceed();
        long end = System.currentTimeMillis() - start;
        log.info("当前类名" + clazz +
                "\n执行的方法名" + method +
                "\n参数为" + args +
                "\n执行时间为" + end/1000.0 + "s");
        return object;
    }

    @AfterThrowing(value = "anyMethod()", throwing = "ex")
    public void exceptionMethod(Exception ex){
        log.info("出现的异常为" + ex.getMessage());
    }
}
